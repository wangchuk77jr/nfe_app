import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import { Avatar, Title, Caption, Drawer, TouchableRipple, Switch } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';

export default function DrawerContent({props,handleLogout}) {

  const navigation = useNavigation();

  const Logout = () => {
    handleLogout();
  };
  return (
    <DrawerContentScrollView {...props}>
      <View style={styles.drawerContent}>
        <View style={styles.userInfoSection}>
            <View style={styles.profileCircle}>
                <Text style={styles.profileInitials}>SW</Text>
            </View>
            <Title style={styles.title}>Sonam Wangchuk</Title>
            <Caption style={styles.caption}>@sonam_w</Caption>
        </View>
        <Drawer.Section style={styles.drawerSection}>
          <DrawerItem
            icon={({ color, size }) => (
              <Icon name="home-outline" color={color} size={size} />
            )}
            label="Home"
            onPress={() => navigation.navigate('Home')}
          />
          <DrawerItem
            icon={({ color, size }) => (
              <Icon name="account-outline" color={color} size={size} />
            )}
            label="Profile"
            onPress={() => navigation.navigate('Profile')}
          />
          <DrawerItem
            icon={({ color, size }) => (
              <Icon name="arrow-collapse-left" color={color} size={size} />
            )}
            label="Logout"
            onPress={Logout}
          />
          {/* Add more DrawerItem components as needed */}
        </Drawer.Section>
        <View style={styles.footer}>
            <Text style={styles.footertext}>Ministry of Education</Text>
            <Text style={styles.footertext}>Copyright © 2024</Text>
        </View>
        
      </View>
    </DrawerContentScrollView>
  );
}

const styles = StyleSheet.create({
    footer:{
        paddingHorizontal:10,
        marginTop:20,
    },
    footertext:{
        textAlign:'center',
        color:'gray'
    },
    profileCircle: {
        width: 50,
        height: 50,
        borderRadius: 30,
        backgroundColor: '#794fed',
        justifyContent: 'center',
        alignItems: 'center',
    },
    profileInitials: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
    },
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 20,
    marginTop: 15,
  },
  title: {
    fontSize: 20,
    marginTop: 3,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  drawerSection: {
    marginTop: 15,
  },
});
