// src/ProfileScreen.js

import React, { useState } from 'react';
import { View, Text, StyleSheet, Modal, TextInput, TouchableOpacity } from 'react-native';
import { Avatar, Title, Caption, Button } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const ProfileScreen = () => {
  const [name, setName] = useState('Sonam Wangchuk');
  const [email, setEmail] = useState('goodman@mail.com');
  const [phone, setPhone] = useState('+975 17368135');
  const [address, setAddress] = useState('Thimphu');

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Avatar.Image 
          source={require('../assets/profile.jpg')}
          size={100}
        />
        <Title style={styles.title}>{name}</Title>
        <Caption style={styles.capt}>English Teacher</Caption>
      </View>

      <View style={styles.infoSection}>
        <View style={styles.row}>
          <Icon name="card-text" color="#777777" size={20}/>
          <View style={styles.textContainer}>
            <Text style={styles.label}>10201004483</Text>
            <Caption style={styles.caption}>CID</Caption>
          </View>
        </View>
        <View style={styles.row}>
          <Icon name="phone" color="#777777" size={20}/>
          <View style={styles.textContainer}>
            <Text style={styles.label}>{phone}</Text>
            <Caption style={styles.caption}>Phone Number</Caption>
          </View>
        </View>
        <View style={styles.row}>
          <Icon name="email" color="#777777" size={20}/>
          <View style={styles.textContainer}>
            <Text style={styles.label}>{email}</Text>
            <Caption style={styles.caption}>Email Address</Caption>
          </View>
        </View>
        <View style={styles.row}>
          <Icon name="map-marker-account" color="#777777" size={25}/>
          <View style={styles.textContainer}>
            <Text style={styles.label}>{address}</Text>
            <Caption style={styles.caption}>Address</Caption>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f4f4f4',
  },
  header: {
    backgroundColor: '#3a3f47',
    alignItems: 'center',
    paddingVertical: 40,
  },
  capt: {
    color: '#fff',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
    color: 'grey',
},
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#ffffff',
    marginTop: 10,
  },
  infoSection: {
    paddingHorizontal: 20,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
  },
  textContainer: {
    flex: 1,
    marginLeft: 20,
  },
  label: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333333',
  },
  editbtn: {
    marginVertical: 20,
  },
  modalOverlay: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    width: '80%',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    marginHorizontal: 10,
  },
  buttonClose: {
    backgroundColor: '#f44336',
    width:'40%'
  },
  buttonSave: {
    backgroundColor: '#4CAF50',
    width:'40%'

  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
  input: {
    height: 40,
    borderColor: '#ddd',
    borderWidth: 1,
    borderRadius: 5,
    width: '100%',
    marginBottom: 20,
    paddingHorizontal: 10,
  },
});

export default ProfileScreen;
